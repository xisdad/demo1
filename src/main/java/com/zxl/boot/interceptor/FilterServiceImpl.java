package com.zxl.boot.interceptor;

import com.alibaba.fastjson.JSON;
import com.zxl.boot.controler.MyOperation;
import com.zxl.boot.dto.SysUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @Auther: zxl
 * @Date: 2021/7/14 11:06
 * @Description: 拦截器：
 */
public class FilterServiceImpl implements HandlerInterceptor {
    private static final Logger log = LoggerFactory.getLogger(FilterServiceImpl.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {



        log.info("preHandle路径"+request.getRequestURI());
        BufferedReader br;
        Map<String,Object> params = new HashMap<String, Object>();
        try {
            br = request.getReader();
            String str, wholeStr = "";
            while((str = br.readLine()) != null){
                wholeStr += str;
            }
            if(!wholeStr.isEmpty()){
                params =  JSON.parseObject(wholeStr,Map.class);
            }
        } catch (IOException e1) {

        }

        //用iterator遍历出来request中的ParameterMap
        String a=request.getParameter("id");
        Map requestMap = request.getParameterMap();
        Iterator it = requestMap.entrySet().iterator();
        StringBuffer stringBuffer = new StringBuffer();
        //show requestParamMap
        while (it.hasNext()) {
            String value = "";
            Map.Entry entry = (Map.Entry) it.next();
            String key = (String) entry.getKey();
            if (entry.getValue() instanceof String[]) {
                value = Arrays.toString((String[]) entry.getValue());
            } else {
                value = (String) entry.getValue();
            }
            stringBuffer.append(key).append("=").append(value);
        }

        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        HandlerMethod h = (HandlerMethod) handler;
        //字面意思返回方法上指定类型的注解
        if (h.getMethodAnnotation(MyOperation.class) != null) {
            System.out.println("用户想执行的操作是:" + h.getMethodAnnotation(MyOperation.class).value());
        }
        System.out.println("用户执行的方法是" + h.getMethod().getName());
        if (h.getMethod().getName().equals("mai")) {
            SysUser input = (SysUser) request.getAttribute("user");
            if (params != null) {
                if (params.get("name").equals("张三")) {
                    response.sendRedirect("/success");
                    return false;
                }
            }
        } else {
            return true;
        }

        return true;
    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.info("postHandle"+modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.info("afterCompletion错误"+ex);
    }
}
