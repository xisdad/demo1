package com.zxl.boot.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Auther: zxl
 * @Date: 2021/7/15 14:44
 * @Description:  拦截器
 */
@Configuration
public class MywebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new FilterServiceImpl())
                // .addPathPatterns("/**")//所有请求都被拦截，包括静态请求
                .excludePathPatterns("/logins","/**")
        ;//放行的请求
    }
}
