package com.zxl.boot.dto;

/**
 * @Auther: zxl
 * @Date: 2021/7/29 14:36
 * @Description:
 */
public class AttrVals {
    private String attr_vals;
    private String attr_id;

    public String getAttr_vals() {
        return attr_vals;
    }

    public void setAttr_vals(String attr_vals) {
        this.attr_vals = attr_vals;
    }

    @Override
    public String toString() {
        return "AttrVals{" +
                "attr_vals='" + attr_vals + '\'' +
                ", attr_id='" + attr_id + '\'' +
                '}';
    }

    public String getAttr_id() {
        return attr_id;
    }

    public void setAttr_id(String attr_id) {
        this.attr_id = attr_id;
    }
}
