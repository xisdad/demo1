package com.zxl.boot.dto;

/**
 * @Auther: zxl
 * @Date: 2021/7/1 00:53
 * @Description:
 */
public class Source {
    private  String id;
    private  String compent;

    public Source(String id, String compent) {
        this.id = id;
        this.compent = compent;
    }
    public Source() {

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompent() {
        return compent;
    }

    public void setCompent(String compent) {
        this.compent = compent;
    }

    @Override
    public String toString() {
        return "Source{" +
                "id='" + id + '\'' +
                ", compent='" + compent + '\'' +
                '}';
    }
}
