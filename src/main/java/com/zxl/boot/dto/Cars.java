package com.zxl.boot.dto;

/**
 * @Auther: zxl
 * @Date: 2021/7/27 17:36
 * @Description:
 */
public class Cars<T> {
    private String hover;
    private String cat_name;
    private String cat_id;
    private T children;

    public T getChildren() {
        return children;
    }

    public void setChildren(T children) {
        this.children = children;
    }

    public String getHover() {
        return hover;
    }

    public void setHover(String hover) {
        this.hover = hover;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    @Override
    public String toString() {
        return "Cars{" +
                "hover='" + hover + '\'' +
                ", cat_name='" + cat_name + '\'' +
                ", cat_id='" + cat_id + '\'' +
                ", children=" + children +
                '}';
    }
}
