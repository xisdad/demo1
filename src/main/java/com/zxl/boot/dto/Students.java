package com.zxl.boot.dto;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Auther: zxl
 * @Date: 2021/7/1 21:12
 * @Description:
 */
@Component
@ConfigurationProperties(prefix = "students")
public class Students {
    private String strname;
    private String six;

    public Students(String strname, String six) {
        this.strname = strname;
        this.six = six;
    }

    public Students() {
    }

    public String getStrname() {
        return strname;
    }

    public void setStrname(String strname) {
        this.strname = strname;
    }

    public String getSix() {
        return six;
    }

    public void setSix(String six) {
        this.six = six;
    }

    @Override
    public String toString() {
        return "Students{" +
                "strname='" + strname + '\'' +
                ", six='" + six + '\'' +
                '}';
    }
}
