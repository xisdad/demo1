package com.zxl.boot.dto;

/**
 * @Auther: zxl
 * @Date: 2021/7/27 01:17
 * @Description:
 */
public class Title<T> {
    private Integer id;
    private String authName;
    private T children;
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAuthName() {
        return authName;
    }

    public void setAuthName(String authName) {
        this.authName = authName;
    }

    public T getChildren() {
        return children;
    }

    public void setChildren(T children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "Title{" +
                "id=" + id +
                ", authName='" + authName + '\'' +
                ", children=" + children +
                ", path='" + path + '\'' +
                '}';
    }
}
