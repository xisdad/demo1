package com.zxl.boot.element;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

/**
 * @Auther: zxl
 * @Date: 2021/8/10 10:46
 * @Description:  通过创建RegistrationBean子类方式实现Filter、Servlet、Listener三大组件
 */
@Configuration
public class MyRegistConfig {
    @Bean
    public ServletRegistrationBean myServlet(){
        Myservlet myServlet=new Myservlet();

        return new ServletRegistrationBean(myServlet,"/myd");
    }

    @Bean
    public FilterRegistrationBean myFilter(){
         Myfilter myfilter=new Myfilter();
        FilterRegistrationBean filterRegistrationBean=new FilterRegistrationBean();
        filterRegistrationBean.setFilter(myfilter);
        filterRegistrationBean.setUrlPatterns(Arrays.asList("/img/**"));
        return filterRegistrationBean;
    }
}
