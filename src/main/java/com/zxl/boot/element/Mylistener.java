package com.zxl.boot.element;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * @Auther: zxl
 * @Date: 2021/8/9 19:30
 * @Description: 监听器
 */
@WebListener
public class Mylistener  implements ServletContextListener {
    private static final Logger log=LoggerFactory.getLogger(Mylistener.class);
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.info("监听项目初始化完成");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("监听项目销毁");
    }
}
