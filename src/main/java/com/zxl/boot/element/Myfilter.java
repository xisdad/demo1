package com.zxl.boot.element;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * @Auther: zxl
 * @Date: 2021/8/9 15:45
 * @Description:  过滤器  手动设置字符集的编码格式  或者 自定义全局配置类定义字符集编码格式
 */

@WebFilter(urlPatterns = "/**")
public class Myfilter implements Filter {
    private static final Logger log=LoggerFactory.getLogger(Myfilter.class);
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("初始化");
    }

    @Override
    public void destroy() {
        log.info("销毁");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.info("工作");
    }
}
