package com.zxl.boot;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @Auther: zxl
 * @Date: 2021/6/29 22:28
 * @Description:
 */
/**
 * 主程序入口
 *@SpringBootApplication 这是一个springboot应用
 */


@ServletComponentScan("com.zxl.boot")
@SpringBootApplication
 @MapperScan({"com.zxl.boot.dao.mapper","com.zxl.boot.sys.dao"})
public class MainApplication {
    private static final Logger LOG = LoggerFactory.getLogger(MainApplication.class);

    public static void main(String[] args){

        // SpringApplication.run(MainApplication.class,args);
        //1.查看返回的IOC容器
        ConfigurableApplicationContext run =SpringApplication.run(MainApplication.class,args);
        //2.容器内的组件实例
        String []names=run.getBeanDefinitionNames();
        for(String name: names){
            LOG.info(name);
        }
        //3.查看实例名
        System.out.println(run.getBean("user"));
        System.out.println(run.getBean("students"));
    }
}
