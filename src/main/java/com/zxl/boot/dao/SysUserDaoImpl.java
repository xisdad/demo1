package com.zxl.boot.dao;


import com.zxl.boot.dao.mapper.SysUserDao;
import com.zxl.boot.dto.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Auther: zxl
 * @Date: 2021/7/29 22:17
 * @Description:
 */
@Service
public class SysUserDaoImpl {
    @Autowired
    SysUserDao sysUserDao;

    public SysUser getSysUser1(String id){
        return sysUserDao.getSysUser1(id);
    }

    public SysUser getSysUser2(String id){
        return sysUserDao.getSysUser1(id);
    }
}
