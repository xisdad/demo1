package com.zxl.boot.dao.mapper;

import com.zxl.boot.dto.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @Auther: zxl
 * @Date: 2021/7/29 21:53
 * @Description:
 */
@Mapper
public interface SysUserDao {
     SysUser getSysUser1(String id);

    @Select("select * from Sys_user where id=#{id}")
    SysUser getSysUser2(String id);
}
