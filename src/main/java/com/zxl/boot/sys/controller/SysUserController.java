package com.zxl.boot.sys.controller;


import com.zxl.boot.sys.bean.SysUserEntity;
import com.zxl.boot.sys.bean.TypeHandler.Encrypt;
import com.zxl.boot.sys.dao.SysUserMapper;
import com.zxl.boot.sys.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zx;
 * @since 2021-09-06
 */
@RestController
@Slf4j
public class SysUserController {


    @Autowired
    ISysUserService iSysUserService;
    @Autowired
    SysUserMapper sysUserMapper;
    @PostMapping("/insert")
    private void insertOne(@RequestBody SysUserEntity sysUserEntity) {
        log.info("***************************************" + sysUserEntity.toString());
        sysUserMapper.insert(new Encrypt(sysUserEntity.getId()),sysUserEntity.getName(),sysUserEntity.getPassword());
    }

    @GetMapping("/select")
    private SysUserEntity select(@RequestParam("id") String id) {
        log.info("*********" + id);
        Encrypt encrypt = new Encrypt();
        encrypt.setValue(id);
        log.info("11111111111"+id.toString());
        return  iSysUserService.select(encrypt);

    }


}
