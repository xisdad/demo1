package com.zxl.boot.sys.service;

import com.zxl.boot.sys.bean.TypeHandler.Encrypt;
import com.zxl.boot.sys.bean.SysUserEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zx;
 * @since 2021-09-06
 */

// public interface ISysUserService extends IService<SysUserEntity> {
//     public int insert(SysUserEntity sysDept);
// }

public interface ISysUserService  {
    // public int insert(Encrypt id,String name ,String password);
    public int insert(Encrypt id,String name ,String password);
    // public int insert(SysUserEntity sysUserEntity);
    public SysUserEntity select(Encrypt id);
}
