package com.zxl.boot.sys.service.impl;

import com.zxl.boot.sys.bean.TypeHandler.Encrypt;
import com.zxl.boot.sys.bean.SysUserEntity;
import com.zxl.boot.sys.dao.SysUserMapper;
import com.zxl.boot.sys.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zx;
 * @since 2021-09-06
 */
// @Service
// public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUserEntity> implements ISysUserService {
//     @Autowired
//     private SysUserMapper sysUserMapper;
//
//     public int insert(SysUserEntity sysDept) {
//         return   sysUserMapper.insert(sysDept);
//     }
// }

@Service
public class SysUserServiceImpl implements ISysUserService {
    @Autowired
    private SysUserMapper sysUserMapper;

    // public int insert(Encrypt id, String name, String password) {
    //     System.out.println("id"+id);
    //     return sysUserMapper.insert( id, name , password);
    // }
    public int insert(Encrypt id, String name, String password) {
        System.out.println("id"+id);
        return sysUserMapper.insert( id, name , password);
    }
    // public int insert(SysUserEntity id) {
    //     System.out.println("id"+id);
    //     return sysUserMapper.insert( id);
    // }



    public SysUserEntity select(Encrypt id) {
        System.out.println("#######"+id.toString());
        return sysUserMapper.select(id);
    }
}
