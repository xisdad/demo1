package com.zxl.boot.sys.bean;

import java.io.Serializable;

/**
 * @Auther: zxl
 * @Date: 2022/1/12 15:14
 * @Description:
 */
public class SysUserEntity  implements Serializable {
    private String id;
    private String name;
    private String password;
    private String age;
    private String roleid;
    private String sex;
    private String depId;

    public SysUserEntity(String name) {
        this.name = name;
    }

    public SysUserEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDepId() {
        return depId;
    }

    public void setDepId(String depId) {
        this.depId = depId;
    }

    @Override
    public String toString() {
        return "SysUserEntity{"  +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", age='" + age + '\'' +
                ", roleid='" + roleid + '\'' +
                ", sex='" + sex + '\'' +
                ", depId='" + depId + '\'' +
                '}';
    }
}
