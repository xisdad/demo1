package com.zxl.boot.sys.bean.TypeHandler;

import java.io.Serializable;

/**
 * @Auther: zxl
 * @Date: 2022/1/13 10:27
 * @Description:
 */
public class Encrypt  implements Serializable {
    private String value;

    public Encrypt(String value) {
        this.value = value;
    }

    public Encrypt() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Encrypt{" +
                "value='" + value + '\'' +
                '}';
    }
}
