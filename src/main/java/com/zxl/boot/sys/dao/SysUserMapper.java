package com.zxl.boot.sys.dao;


import com.zxl.boot.sys.bean.SysUserEntity;
import com.zxl.boot.sys.bean.TypeHandler.Encrypt;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx;
 * @since 2021-09-06
 */
// @Mapper
// public interface SysUserMapper extends BaseMapper<SysUserEntity> {
//     public int insert(SysUserEntity sysUserEntity);
// }

@Mapper
public interface SysUserMapper  {
    // public int insert(@Param("id") Encrypt id,@Param("name") String name,@Param("password") String password);

    // public int insert(SysUserEntity sysUserEntity);

    public int insert(@Param("id") Encrypt id, @Param("name") String name, @Param("password") String password);

    public SysUserEntity select(@Param("id") Encrypt id);
}
