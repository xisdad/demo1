package com.zxl.boot.util;

/**
 * @Auther: zxl
 * @Date: 2021/7/27 01:04
 * @Description:
 */
public class JesonResult<T> {
    private Integer code;
    private String msg;
    private T data;
    private T meta;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public T getMeta() {
        return meta;
    }

    public void setMeta(T meta) {
        this.meta = meta;
    }

    public JesonResult(Integer code, String msg, T data, T meta) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.meta = meta;
    }

    public JesonResult() {
    }

    @Override
    public String toString() {
        return "JesonResult{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                ", meta=" + meta +
                '}';
    }

    public static JesonResult success(){
        JesonResult jesonResult = new JesonResult();
        jesonResult.setCode(EnumsAction.SUCCESS.getCode());
        jesonResult.setMsg(EnumsAction.SUCCESS.getMsg());
        return jesonResult;
    }


    public static JesonResult error(){
        JesonResult jesonResult = new JesonResult();
        jesonResult.setCode(EnumsAction.ERROR.getCode());
        jesonResult.setMsg(EnumsAction.ERROR.getMsg());
        return jesonResult;
    }

    public JesonResult code(Integer code){
        this.setCode(code);
        return this;
    }

    public JesonResult message(String message){
        this.setMsg(message);
        return this;
    }

    public JesonResult data(T data){
        this.setData(data);
        return this;
    }

    public JesonResult meta(T meta){
        this.setMeta(meta);
        return this;
    }

}
