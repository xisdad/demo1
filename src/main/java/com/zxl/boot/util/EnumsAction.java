package com.zxl.boot.util;

/**
 * @Auther: zxl
 * @Date: 2021/7/26 23:17
 * @Description:
 */
public enum EnumsAction {
    SUCCESS(200,"成功"),
    ERROR(400,"错误");

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    protected Integer code;
    protected String msg;
    EnumsAction(Integer code,String msg){
        this.code = code;
        this.msg =msg;
    }
}
