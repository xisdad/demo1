package com.zxl.boot.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: zxl
 * @Date: 2021/7/26 23:18
 * @Description:
 */

public class ResponseFormat {
    private Integer code;
    private String msg;
    private Map<String,Object> data = new HashMap<String,Object>();
    private Map<String,Object> meta = new HashMap<String,Object>();
    private List<Object> dat = new ArrayList<Object>();

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public Map<String, Object> getMeta() {
        return meta;
    }

    public void setMeta(Map<String, Object> meta) {
        this.meta = meta;
    }

    public ResponseFormat(Integer code, String msg, Map<String, Object> data, Map<String, Object> meta) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.meta = meta;
    }


    public ResponseFormat() {
    }

    @Override
    public String toString() {
        return "ResponseFormat{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                ", meta=" + meta +
                '}';
    }

    public static ResponseFormat success(){
        ResponseFormat responseFormat = new ResponseFormat();
        responseFormat.setCode(EnumsAction.SUCCESS.getCode());
        responseFormat.setMsg(EnumsAction.SUCCESS.getMsg());
        return responseFormat;
    }


    public static ResponseFormat error(){
        ResponseFormat responseFormat = new ResponseFormat();
        responseFormat.setCode(EnumsAction.ERROR.getCode());
        responseFormat.setMsg(EnumsAction.ERROR.getMsg());
        return responseFormat;
    }

    public ResponseFormat code(Integer code){
        this.setCode(code);
        return this;
    }

    public ResponseFormat message(String message){
        this.setMsg(message);
        return this;
    }

    public ResponseFormat data(String key,Object value){
        this.data.put(key,value);
        return this;
    }

    public ResponseFormat data(Map<String,Object> map){
        this.data = map;
        return this;
    }

    public ResponseFormat meta(String key,Object value){
        this.meta.put(key,value);
        return this;
    }

    public ResponseFormat meta(Map<String,Object> map){
        this.meta = map;
        return this;
    }

}
