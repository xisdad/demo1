package com.zxl.boot.fileUpload;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @Auther: zxl
 * @Date: 2021/8/16 14:34
 * @Description:
 */
@Component
@PropertySource("classpath:application.yml")
@ConfigurationProperties(prefix = "upload")
public class FileLoad {


    private Local local = new Local();
    private Qiniu qiniu = new Qiniu();
    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public Qiniu getQiniu() {
        return qiniu;
    }

    public void setQiniu(Qiniu qiniu) {
        this.qiniu = qiniu;
    }



    public class Local {

    }

    public class Qiniu {
        public String getDomain() {
            return domain;
        }

        public void setDomain(String domain) {
            this.domain = domain;
        }

        public String getAccessKey() {
            return accessKey;
        }

        public void setAccessKey(String accessKey) {
            this.accessKey = accessKey;
        }

        public String getSecretKey() {
            return secretKey;
        }

        public void setSecretKey(String secretKey) {
            this.secretKey = secretKey;
        }

        public String getBucket() {
            return bucket;
        }

        public void setBucket(String bucket) {
            this.bucket = bucket;
        }

        /**
         * 域名
         */
        private String domain;

        /**
         * 从七牛云获取accessKey和secretKey
         */
        private String accessKey;

        private String secretKey;

        /**
         * 存储空间名
         */
        private String bucket;

    }
}
