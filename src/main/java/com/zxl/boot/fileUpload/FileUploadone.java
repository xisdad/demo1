package com.zxl.boot.fileUpload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @Auther: zxl
 * @Date: 2021/8/16 14:04
 * @Description:
 */
@Controller
public class FileUploadone {
    private static final Logger log = LoggerFactory.getLogger(FileUploadone.class);

    @PostMapping("/upload")
    public String fileone(@RequestParam("email") String stpara,
                          @RequestPart("userImg") MultipartFile fileImg,
                          @RequestPart("photos") MultipartFile[] photo) throws IOException {

        log.info("文件大小{},文件类型{}", fileImg.getSize(), fileImg.getName());
        if (!fileImg.isEmpty()) {
            //上传到七牛云服务器，或者本地磁盘
            //1选择到本地磁盘
            String filename=fileImg.getOriginalFilename();
            fileImg.transferTo(new File("H:\\work\\spring5\\demo1"));
            // 2七牛云服务器

        }
        return "";
    }

}
