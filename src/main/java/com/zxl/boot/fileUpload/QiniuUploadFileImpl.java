package com.zxl.boot.fileUpload;

import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

/**
 * @Auther: zxl
 * @Date: 2021/8/16 17:33
 * @Description:
 */
public class QiniuUploadFileImpl implements QiniuUploadFile {

    private FileLoad.Qiniu fileLoad;

    //构造一个带指定Region对象的配置类
    private Configuration cfg = new Configuration(Region.region2());

    private UploadManager uploadManager= new UploadManager(cfg);

    public QiniuUploadFileImpl(FileLoad.Qiniu fileLoad) {
        this.fileLoad = fileLoad;
    }


    @Override
    public String qiniuUploadFile(MultipartFile file) {
        Auth auth = Auth.create(fileLoad.getAccessKey(), fileLoad.getSecretKey());
        String token = auth.uploadToken(fileLoad.getBucket());
        try {
            String originalFilename = file.getOriginalFilename();
            // 文件后缀
            String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
            String fileKey = UUID.randomUUID().toString()  + suffix;
            Response response = uploadManager.put(file.getInputStream(), fileKey, token, null, null);
            return fileLoad.getDomain() +  fileKey;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "error";

    }
}
