package com.zxl.boot.fileUpload;

import org.springframework.web.multipart.MultipartFile;

/**
 * @Auther: zxl
 * @Date: 2021/8/16 17:31
 * @Description:
 */
public interface QiniuUploadFile {
    public String qiniuUploadFile(MultipartFile file);
}
