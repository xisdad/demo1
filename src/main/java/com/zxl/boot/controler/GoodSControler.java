package com.zxl.boot.controler;

import com.zxl.boot.dao.SysUserDaoImpl;
import com.zxl.boot.dao.mapper.SysDeptDao;
import com.zxl.boot.dto.Goods;
import com.zxl.boot.util.JesonResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: zxl
 * @Date: 2021/7/29 16:14
 * @Description:
 */
@Controller
@CrossOrigin
public class GoodSControler {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    SysUserDaoImpl sysUserDaoImpl;
    @Autowired
    SysDeptDao sysDeptDao;
    private static final Logger log = LoggerFactory.getLogger(RequestControler.class);

    @CrossOrigin
    @ResponseBody
    @PostMapping("/api/private/v1/goods")
    public JesonResult atts(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> map2 = new HashMap<>();

        map2.put("status", 200);
        map2.put("msg", "");
        return JesonResult.success().meta(map2);
    }

    @CrossOrigin
    @ResponseBody
    @GetMapping("/api/private/v1/goods")
    public JesonResult attsss(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        Map<String, Object> map2 = new HashMap<>();
        map2.put("msg", "");
        map2.put("status", 200);
        List<Goods> list = new ArrayList<>();
        String sql = "sELECT * FROM goods ";
        RowMapper<Goods> rowMapper = new BeanPropertyRowMapper<Goods>(Goods.class);
        list = jdbcTemplate.query(sql, rowMapper);
        log.info(list.toString());

        // SysUser sysUser= sysUserDaoImpl.getSysUser1("3");
        // log.info(sysUser.toString());
        //
        // SysUser sysUser2= sysUserDaoImpl.getSysUser2("5");
        // log.info(sysUser2.toString());

        // List<SysDept> dept=sysDeptDao.selectList(null);

        // log.info(dept.toString());
        Map<String, Object> map3 = new HashMap<>();
        map3.put("goods", list);
        return JesonResult.success().data(map3).meta(map2);
    }
}
