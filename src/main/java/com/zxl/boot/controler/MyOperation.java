package com.zxl.boot.controler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Auther: zxl
 * @Date: 2021/7/14 11:28
 * @Description:
 */
    @Retention(RetentionPolicy.RUNTIME)//RUNTIME
    @Target(ElementType.METHOD)
    public @interface MyOperation {
        String value() default "xx";
            //默认为空，因为名字是value，实际操作中可以不写"value="

}
