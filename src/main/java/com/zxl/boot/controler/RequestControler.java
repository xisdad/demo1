package com.zxl.boot.controler;

import com.zxl.boot.dto.AttrVals;
import com.zxl.boot.dto.Cars;
import com.zxl.boot.dto.SysUser;
import com.zxl.boot.dto.Title;
import com.zxl.boot.util.JesonResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: zxl
 * @Date: 2021/7/21 16:57
 * @Description:
 */
@Controller
@CrossOrigin
public class RequestControler {
    private static final Logger log = LoggerFactory.getLogger(RequestControler.class);
    @Autowired
    JdbcTemplate jdbcTemplate;

    @CrossOrigin
    @ResponseBody
    @GetMapping("/api/private/v1/menus")
    public JesonResult att(HttpServletRequest request, HttpServletResponse response) {

        Map<String, Object> map2 = new HashMap<>();
        // Map<String, Object> map1 = new HashMap<>();
        // map1.put("101", "菜单1");
        // map1.put("102", "菜单2");
        // map1.put("103", "菜单3");
        // map1.put("125", "菜单4");
        // map1.put("145", "菜单5");
        // map1.put("147", "菜单6");
        List<Title> list1 = new ArrayList<>();
        Title title = new Title();
        title.setId(102);
        title.setAuthName("商品管理");
        List<Object> objects2 = new ArrayList<Object>();
        Title title1 = new Title();
        title1.setId(1021);
        title1.setAuthName("添加商品");
        title1.setPath("goods/add");
        objects2.add(title1);
        title1 = new Title();
        title1.setId(1022);
        title1.setAuthName("商品分类");
        title1.setPath("categories");
        objects2.add(title1);
        title1 = new Title();
        title1.setId(1023);
        title1.setAuthName("商品列表");
        title1.setPath("goods");
        objects2.add(title1);
        title1 = new Title();
        title1.setId(1024);
        title1.setAuthName("参数列表");
        title1.setPath("params");
        objects2.add(title1);
        title.setChildren(objects2);
        list1.add(title);
        title = new Title();
        title.setId(103);
        title.setAuthName("订单管理");
        //
        objects2 = new ArrayList<Object>();
        title1 = new Title();
        title1.setId(1031);
        title1.setAuthName("订单列表");
        title1.setPath("orders");
        objects2.add(title1);
        title.setChildren(objects2);
        //
        list1.add(title);

        title = new Title();
        title.setId(101);
        title.setAuthName("角色权限");
        //rights
        objects2 = new ArrayList<Object>();
        title1 = new Title();
        title1.setId(1011);
        title1.setAuthName("权限列表");
        title1.setPath("rights");
        objects2.add(title1);
        title.setChildren(objects2);
        title1 = new Title();
        title1.setId(1012);
        title1.setAuthName("角色列表");
        title1.setPath("roles");
        objects2.add(title1);
        title.setChildren(objects2);
        //
        list1.add(title);
        title = new Title();
        title.setId(125);
        title.setAuthName("数据统计");
        //
        objects2 = new ArrayList<Object>();
        title1 = new Title();
        title1.setId(1251);
        title1.setAuthName("数据报表");
        title1.setPath("reports");
        objects2.add(title1);
        title.setChildren(objects2);
        //
        list1.add(title);
        title = new Title();
        title.setId(145);
        title.setAuthName("用户管理");
        //
        objects2 = new ArrayList<Object>();
        title1 = new Title();
        title1.setId(1451);
        title1.setAuthName("用户列表");
        title1.setPath("users");
        objects2.add(title1);
        title.setChildren(objects2);
        //
        list1.add(title);
        map2.put("status", 200);
        map2.put("msg", "");
        return JesonResult.success().data(list1).meta(map2);
    }

    @CrossOrigin
    @ResponseBody
    @GetMapping("/api/private/v1/categories")
    public JesonResult attrr(HttpServletRequest request, HttpServletResponse response) {
        RowMapper<SysUser> rowMapper = new BeanPropertyRowMapper<SysUser>(SysUser.class);
        SysUser sysUser = jdbcTemplate.queryForObject("SELECT * FROM sys_user where id=1", rowMapper);
        log.info("****************" + sysUser.toString());
        Map<String, Object> map2 = new HashMap<>();
        map2.put("status", 200);
        List<Object> list1 = new ArrayList<Object>();
        Cars cars = new Cars();

        cars.setCat_name("商品1");
        cars.setCat_id("1");
        list1.add(cars);

        cars = new Cars();
        cars.setCat_name("商品2");
        cars.setCat_id("2");

        List<Object> objects = new ArrayList<Object>();
        Cars carsChild = new Cars();
        carsChild.setCat_id("21");
        carsChild.setCat_name("商品2-1");

        List<Object> objects2 = new ArrayList<Object>();
        Cars carsChild2 = new Cars();
        carsChild2.setCat_id("211");
        carsChild2.setCat_name("商品2-1-1");
        objects2.add(carsChild2);
        carsChild2 = new Cars();
        carsChild2.setCat_id("212");
        carsChild2.setCat_name("商品2-1-2");
        objects2.add(carsChild2);
        carsChild2 = new Cars();
        carsChild2.setCat_id("213");
        carsChild2.setCat_name("商品2-1-3");
        objects2.add(carsChild2);
        carsChild.setChildren(objects2);

        objects.add(carsChild);


        carsChild = new Cars();
        carsChild.setCat_id("22");
        carsChild.setCat_name("商品2-2");
        objects.add(carsChild);
        carsChild = new Cars();
        carsChild.setCat_id("23");
        carsChild.setCat_name("商品2-3");
        objects.add(carsChild);

        cars.setChildren(objects);
        list1.add(cars);

        cars = new Cars();
        cars.setCat_name("商品3");
        cars.setCat_id("3");
        list1.add(cars);

        cars = new Cars();
        cars.setCat_name("商品21");
        cars.setCat_id("1");
        list1.add(cars);

        return JesonResult.success().data(list1).meta(map2);
    }

    @CrossOrigin
    @ResponseBody
    @GetMapping("/api/private/v1/categories/{id}/attributes")
    public JesonResult attrrg(@PathVariable("id") Integer id
            , @RequestParam(value = "sel", required = false) String sel
    ) {
        Map<String, Object> map2 = new HashMap<>();
        map2.put("status", 200);

        List<Object> list = new ArrayList<>();
        AttrVals attrVals = new AttrVals();
        attrVals.setAttr_id("2121");
        attrVals.setAttr_vals("帆布鞋");
        list.add(attrVals);
        attrVals = new AttrVals();
        attrVals.setAttr_id("2122");
        attrVals.setAttr_vals("运动鞋");
        list.add(attrVals);
        attrVals = new AttrVals();
        attrVals.setAttr_id("2123");
        attrVals.setAttr_vals("版鞋");
        list.add(attrVals);
        return JesonResult.success().data(list).meta(map2);
    }

    @ResponseBody
    @GetMapping("/success")
    public Map<String, Object> zf(HttpServletRequest request) {

        Map<String, Object> maps = new HashMap<>();
        maps.put("ss", "1");
        maps.put("ss", "2");
        return maps;
    }

    @CrossOrigin
    @ResponseBody
    @PostMapping(value = {"/", "/logins", "/api/private/v1/login"})
    public JesonResult login(HttpServletRequest request, HttpServletResponse response
    ) {
        log.info("ssssss");
        Map<String, Object> map1 = new HashMap<>();
        Map<String, Object> map2 = new HashMap<>();
        map1.put("token", "SFG98Kol46478ZXL");
        map2.put("status", 200);
        // return ResponseFormat.success().data(map1).meta(map2);
        return JesonResult.success().data(map1).meta(map2);
    }

    @PostMapping(value = {"/login"})
    public String index(@RequestBody SysUser sysUser, HttpSession session, Model model) {
        session.setAttribute("user", sysUser);
        if (!sysUser.getName().isEmpty() && !sysUser.getPassword().isEmpty() && sysUser.getPassword().equals("111")) {
            System.out.println("login1");
            return "index";
        }
        System.out.println("login2");
        return "redirect:/login";
    }


    @PostMapping("/index")
    public String mai(HttpSession session, Model model, HttpServletRequest httpServletRequest) {
        Object logUser = session.getAttribute("user");
        if (logUser != null) {
            System.out.println("index1");
            return "redirect:/login";
        } else {
            httpServletRequest.setAttribute("msg", "登录失败");
            httpServletRequest.setAttribute("stat", "111111");
            System.out.println("没有登录，请登录");
            System.out.println(httpServletRequest.getAttribute("msg"));
            return "forward:/logins";
        }
    }

    @PostMapping
    @ResponseBody
    public String ycdl(HttpServletRequest httpServletRequest, @MatrixVariable("ss") String a) {


        return "";
    }
}
