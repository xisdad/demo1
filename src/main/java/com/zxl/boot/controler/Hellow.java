package com.zxl.boot.controler;

import com.zxl.boot.dto.Students;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: zxl
 * @Date: 2021/6/29 22:32
 * @Description:
 * @ResponseBody 这个类下面的所有请求都是返回给浏览器响应的
 * @ResponseBody 这个类下面的所有请求都是返回给浏览器响应的
 */
/*@Controller
@ResponseBody
*//**
 *@ResponseBody 这个类下面的所有请求都是返回给浏览器响应的
 */


/**
 *@RestController :包含了@Controller  @ResponseBody
 */
@RestController
public class Hellow {
    @Autowired
    Students students;


    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public String hell() {
        return "post请求";
    }

    @RequestMapping(value = "/user", method = RequestMethod.DELETE)
    public String hell1() {
        return "delete请求";
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public Students sd() {
        return students;
    }

    @RequestMapping(value = "/user", method = RequestMethod.PUT)
    public String hell2() {
        return "put请求";
    }

    @RequestMapping("/filt")
    @MyOperation("用户修改")//主要看这里
    @ResponseBody
    public String test(String id) {
        return "demo!" + id;
    }

    @GetMapping("/sys/{id}/owner/{name}") //http://localhost:8080/sys/15/owner/张三
    public Map<String, Object> getSys(@PathVariable("id") Integer id,
                                      @PathVariable("name") String name,
                                      @PathVariable Map<String, String> tpv,
                                      @RequestHeader Map<String, String> headMap,
                                      @CookieValue("JSESSIONID") String ck
    ) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("name", name);
        map.put("tpv", tpv);
        map.put("headMap", headMap);
        map.put("ck", ck);
        return map;
    }

    @ResponseBody
    @GetMapping("/sys/{id}/owner") //http://localhost:8080/sys/15/owner?user_name=张三
    public String success(
            @PathVariable("id") Integer id,
            @RequestParam(value = "user_name", required = false) String userName) {
        return "success";
    }

    @ResponseBody
    @PostMapping("/save")
    public Map<String, Object> postSave(@RequestBody Map<String, String> map) {
        Map<String, Object> maps = new HashMap<>();

        maps.put("name1", map.get("name"));
        maps.put("name2", map.get("gg"));
        maps.put("mm", map.get("wd"));
        return maps;
    }


    // 矩阵变量
    // /search/car;name=zz;size=20;much=100
    @GetMapping("/search/car")
    public Map<String, Object> ser(@MatrixVariable("name") String name,
                                   @MatrixVariable("size") Integer size,
                                   @MatrixVariable("much") Integer much) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("a", name);
        maps.put("b", size);
        maps.put("c", much);

        return maps;
    }

}
