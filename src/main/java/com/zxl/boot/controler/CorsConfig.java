package com.zxl.boot.controler;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

/**
 * @Auther: zxl
 * @Date: 2021/7/26 17:41
 * @Description:  拦截器  WebMvcConfigurationSupport
 */
@Configuration
public class CorsConfig  extends WebMvcConfigurationSupport {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        //
        registry.addMapping("/**") .allowedOrigins("http://localhost:8081").allowedOrigins("http://localhost:8080");
    }

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);
    }

    //重写addResourceHandlers方法，添加静态资源路径
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/resources/")
                .addResourceLocations("classpath:/static/")
                .addResourceLocations("classpath:/public/")
        ;
        super.addResourceHandlers(registry);
    }




}
