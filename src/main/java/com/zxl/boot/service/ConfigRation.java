package com.zxl.boot.service;

import com.zxl.boot.dto.Students;
import com.zxl.boot.dto.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * @Auther: zxl
 * @Date: 2021/7/1 00:38
 * @Description:
 */
@ImportResource("classpath:bean.xml")
@Configuration(proxyBeanMethods = false)
public class ConfigRation {

    @Bean
    public User user(){
        return new User("张三",15);
    }

}
