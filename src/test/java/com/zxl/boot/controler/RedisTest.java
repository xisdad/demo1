package com.zxl.boot.controler;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @Auther: zxl
 * @Date: 2021/8/23 15:52
 * @Description: redis连接测试
 */
@SpringBootTest
public class RedisTest {
  static  JedisPool pool;

    @Test
    public void testRedis(){
        JedisPool pool;
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(10);
        pool = new JedisPool(jedisPoolConfig, "localhost");
        System.out.println("连接池初始化成功");
        // 获取连接
        Jedis jedis = pool.getResource();
        jedis.set("zxl", "张修龙");
        pool.destroy();
        System.out.println("连接池关闭");
    }



}
