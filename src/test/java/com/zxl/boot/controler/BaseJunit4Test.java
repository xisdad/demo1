package com.zxl.boot.controler;

import com.zxl.boot.MainApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Auther: zxl
 * @Date: 2021/8/20 19:58
 * @Description:
 */
@RunWith(SpringRunner.class) //使用junit4进行测试
@SpringBootTest(classes = MainApplication.class)
public class BaseJunit4Test {
}
