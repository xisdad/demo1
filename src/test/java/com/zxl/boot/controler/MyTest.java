package com.zxl.boot.controler;

import com.zxl.boot.dto.Students;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @Auther: zxl
 * @Date: 2021/8/21 11:06
 * @Description:
 */
@DisplayName("junit5功能注解测试")
@SpringBootTest
public class MyTest {

    private static final Logger log = LoggerFactory.getLogger(RequestControler.class);
    @Autowired
    Students students;
    @DisplayName("自定义类测试用例一")
    @Test
    public void test(){
        log.info("容器里的bean实例{}",students);
    }
    @BeforeEach
    void before(){
        log.info("开始测试");
    }
}
