package com.zxl.boot.controler;

import com.zxl.boot.dto.Students;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Hellow Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>08/20/2021</pre>
 */
public class HellowTest extends BaseJunit4Test {
    private static final Logger log = LoggerFactory.getLogger(RequestControler.class);
    @Autowired
    Students students;

    /**
     * Method: hell()
     */
    @Test
    public void testHell() throws Exception {
        log.info("容器里的bean实例{}",students);
    }

    /**
     * Method: hell1()
     */
    @Test
    public void testHell1() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: sd()
     */
    @Test
    public void testSd() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: hell2()
     */
    @Test
    public void testHell2() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: test(String id)
     */
    @Test
    public void testTest() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getSys(@PathVariable("id") Integer id, @PathVariable("name") String name, @PathVariable Map<String, String> tpv, @RequestHeader Map<String, String> headMap, @CookieValue("JSESSIONID") String ck)
     */
    @Test
    public void testGetSys() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: postSave(@RequestBody Map<String, String> map)
     */
    @Test
    public void testPostSave() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: ser(@MatrixVariable("name") String name, @MatrixVariable("size") Integer size, @MatrixVariable("much") Integer much)
     */
    @Test
    public void testSer() throws Exception {
        //TODO: Test goes here...
    }


} 
